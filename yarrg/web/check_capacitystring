<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component simply defines how to interpret capacities.

</%doc>
<%method execute>
<%args>
$string
$dbh
$debugf
</%args>
<%perl>

my $commodsth;

my @mv_names= qw(mass volume);
my @mv_units= qw(kg l);

my (@mv)= (undef,undef);
return ('',@mv) unless $string =~ m/\S/;

my @canon= ();
my ($signum,$signopstr)= (+1,undef);
my $show_answer=0;
my $first_term=1;
my $last_signopstr= 'NONE';

my $canon_numeric= sub {
	my ($val,$mvi) = @_;
	sprintf "%g%s", $val, $mv_units[$mvi];
};

my $parse_values= sub {
  local ($_) = @_;
  $debugf->("TERM VALUES '$_'");
  $_ .= ' ';
  my $def= sub {
	my ($mvi,$val) = @_;
	if ($first_term) {
		expected_error("Initial term specifies".
				" $mv_names[$mvi] more than once.")
			if defined $mv[$mvi];
		$mv[$mvi]= $val;
	} else {
		expected_error("Cannot add or subtract mass to/from volume")
			unless defined $mv[$mvi];
		$mv[$mvi] += $signum * $val;
	}
	push @canon, $canon_numeric->($val,$mvi);
  };
  while (m/\S/) {
	$debugf->("VALUE '$_'");
	my $iqtyrex= '[1-9] \d{0,8}';
	my $fqtyrex= '\d{1,9} \. \d{0,3} |' . $iqtyrex;
	if    (s/^( $fqtyrex ) \s* kg \s+ //xo) { $def->(0, $1          ); }
	elsif (s/^( $fqtyrex ) \s* t  \s+ //xo) { $def->(0, $1 * 1000.0 ); }
	elsif (s/^( $fqtyrex ) \s* l  \s+ //xo) { $def->(1, $1          ); }
	elsif (s/^( $fqtyrex ) \s* kl \s+ //xo) { $def->(1, $1 * 1000.0 ); }
	elsif (s/^( $iqtyrex ) \s* ([a-z ]+) \s+ //ixo) {
		my ($qty,$spec) = ($1,$2);
		$debugf->("VALUE COMMOD $qty '$spec'");
		expected_error("Capacity specification must start with".
			       " ship size or amount with units")
			if $first_term;
	        $commodsth ||=
		    $dbh->prepare("SELECT commodname,unitmass,unitvolume
				     FROM commods WHERE commodname LIKE ?");
	        my ($emsg,$commod,@umv)=
		    dbw_lookup_string($spec,$commodsth,1,0,0,
			"No commodity or unit matches ".escerrq($spec),
			"Ambiguous commodity (or unit) ".escerrq($spec),
			undef);
		expected_error($emsg) if defined $emsg;
		$debugf->("VALUE COMMOD FOUND '$commod' @umv");
		foreach my $mvi (0,1) {
		       next unless defined $mv[$mvi];
		       $mv[$mvi] += $signum * $qty * $umv[$mvi] * 0.001;
		}
		push @canon, sprintf "%d", $qty;
		push @canon, $commod;
	} else {
		s/\s+$//;
		expected_error("Did not understand value ".
			escerrq($_));
	}
  }
};

my $parse_term= sub {
	local ($_) = @_;
	$debugf->("TERM '$_' signum=$signum");
	s/^\s+//; s/\s+$//;
	expected_error("empty term in capacity") unless m/\S/;
	if (m/^\s*(\d{1,2}(?:\.\d{0,4})?)\%\s*$/) {
		$debugf->("TERM PERCENT $1");
		expected_error("percentage may not be first item")
			if $first_term;
		my $pct= 100.0 + $signum * $1;
		foreach (@mv) {
			next unless defined;
			$_ *= $pct / 100.0;
		}
		push @canon, sprintf "%g%%", $pct;
	} elsif (!m/[^a-z ]/i) {
		$debugf->("TERM NAME");
		expected_error("Name (should be unit or commodity)".
				escerrq($_).
				" without preceding quantity")
			unless $first_term;
		my $sth= $dbh->prepare("SELECT name,mass,volume".
				       "  FROM vessels WHERE name LIKE ?");
		my ($emsg,$ship,@smv)=
		    dbw_lookup_string($_,$sth,1,1,2,
				"Ship name ".escerrq($_)." not understood.",
				"Too many matching ship types.",
				sub { "Ambiguous - could be $_[1]" });
		expected_error($emsg) if defined $emsg;
		$debugf->("TERM NAME SHIP '$ship' @smv");
		$show_answer= 1;
		@mv = @smv;
		push @canon, $ship;
	} else {
		$parse_values->($_);
	}
	$first_term= 0;
};

while ($string =~ s/^(.*?)(\bminus\b|-|\bplus\b|\+)//i) {
	my ($lhs)= ($1);
	my @nextsign= $2 =~ m/^p|^\+/ ? (+1,'+') : (-1,'-');
	$show_answer= 1;
	$debugf->("GROUP S='$2'");
	$parse_term->($lhs);
	($signum,$signopstr)= @nextsign;
	push @canon, ($last_signopstr=$signopstr)
		if $signopstr ne $last_signopstr;
}
$parse_term->($string);

my $canon= join ' ', @canon;

if ($show_answer) {
	$canon .= "  [=";
	foreach my $mvi (0,1) {
		next unless defined $mv[$mvi];
		$canon .= ' '.$canon_numeric->($mv[$mvi], $mvi);
	}
	$canon .= "]";
}

$debugf->("FINISHING canon='$canon'");

foreach my $mvi (0,1) {
	next unless defined $mv[$mvi];
	next if $mv[$mvi] >= 0;
	expected_error(sprintf "%s limit is negative: %s",
		ucfirst($mv_names[$mvi]), $canon_numeric->($mv[$mvi], $mvi));
}

return ($canon, @mv);

</%perl>
</%method>
