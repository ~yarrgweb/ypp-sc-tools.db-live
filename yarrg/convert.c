/*
 * yarrg main program: argument parsing etc.
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "convert.h"

const char *get_vardir(void) { return "."; }
const char *get_libdir(void) { return "."; }


enum outmodekind {
  omk_unset, omk_raw, omk_str, omk_upload_yarrg, omk_upload_pctb
};

typedef struct {
  enum outmodekind kind; /* unset is sentinel */
  const char *str; /* non-0 iff not unset or raw */
} OutputMode;

static OutputMode o_outmodes[10];

static char *o_screenshot_fn;
static const char *o_serv_pctb, *o_serv_yarrg;
static const char *o_serv_dict_fetch, *o_serv_dict_submit;

const char *o_resolver= "./dictionary-manager";
FILE *screenshot_file;
const char *o_ocean, *o_pirate;
int o_quiet;

static pid_t screenshot_compressor=-1;

enum mode o_mode= mode_all;
enum flags o_flags=
   ff_charset_allowedit |
   ff_dict_fetch|ff_dict_submit|ff_dict_pirate;

static void vbadusage(const char *fmt, va_list) FMT(1,0) NORET;
static void vbadusage(const char *fmt, va_list al) {
  fputs("bad usage: ",stderr);
  vfprintf(stderr,fmt,al);
  fputc('\n',stderr);
  exit(12);
}
DEFINE_VWRAPPERF(static, badusage, NORET);

static void open_screenshot_file(int for_write) {
  if (!fnmatch("*.gz",o_screenshot_fn,0)) {
    int mode= for_write ? O_WRONLY|O_CREAT|O_TRUNC : O_RDONLY;
    sysassert(! gzopen(o_screenshot_fn, mode, &screenshot_file,
		       &screenshot_compressor, "-1") );
  } else {
    screenshot_file= fopen(o_screenshot_fn, for_write ? "w" : "r");
    if (!screenshot_file)
      fatal("could not open screenshots file `%s': %s",
	    o_screenshot_fn, strerror(errno));
  }
}

static void run_analysis(void) {
  FILE *tf;
  OutputMode *om;

  sysassert( tf= tmpfile() );
  progress("running recognition...");
  analyse(tf);

  if (o_flags & ff_upload) {
    if (o_flags & ff_singlepage)
      fatal("Recognition successful, but refusing to upload partial data\n"
	    " (--single-page specified).  Specify an output mode?");
  }

  for (om=o_outmodes; om->kind != omk_unset; om++) {
    sysassert( fseek(tf,0,SEEK_SET) == 0);

    progress_log("processing results (--%s)...", om->str);
    pid_t processor;
    sysassert( (processor= fork()) != -1 );

    if (!processor) {
      sysassert( dup2(fileno(tf),0) ==0 );
      if (om->kind==omk_raw) {
	execlp("cat","cat",(char*)0);
	sysassert(!"execute cat");
      } else {
	EXECLP_HELPER("commod-results-processor", om->str, (char*)0);
      }
    }

    waitpid_check_exitstatus(processor, "output processor/uploader", 0);
  }
  
  fclose(tf);
  progress_log("all complete.");
}

static void rsync_core(const char *stem, const char *suffix,
		       const char *zopt) {
  pid_t fetcher;

  progress("Updating dictionary %s...",stem);

  sysassert( (fetcher= fork()) != -1 );
  if (!fetcher) {
    const char *rsync= getenv("YPPSC_YARRG_RSYNC");
    if (!rsync) rsync= "rsync";
  
    const char *src= getenv("YPPSC_YARRG_DICT_UPDATE");
    char *remote= masprintf("%s/master-%s.txt%s", src, stem, suffix);
    char *local= masprintf("_master-%s.txt%s", stem, suffix);
    if (DEBUGP(rsync))
      fprintf(stderr,"executing rsync to fetch %s to %s\n",remote,local);
    char *opts= masprintf("-Lt%s%s",
			  zopt,
			  DEBUGP(rsync) ? "v" : "");
    execlp(rsync, "rsync",opts,"--",remote,local,(char*)0);
    sysassert(!"exec rsync failed");
  }

  waitpid_check_exitstatus(fetcher, "rsync", 0);
}

void fetch_with_rsync_gz(const char *stem) { rsync_core(stem,".gz",""); }
void fetch_with_rsync(const char *stem) { rsync_core(stem,"","z"); }

static void get_timestamp(void) {
  FILE *tf;
  pid_t child;
  sysassert( tf= tmpfile() );

  sysassert( (child= fork()) != -1 );
  if (!child) {
    sysassert( dup2(fileno(tf),1)==1 );
    EXECLP_HELPER("database-info-fetch","timestamp",(char*)0);
  }
  waitpid_check_exitstatus(child,"timestamp request",0);

  sysassert( fseek(tf,0,SEEK_SET) == 0 );
  static char lbuf[30];
  int l= fread(lbuf,1,sizeof(lbuf),tf);
  sysassert( !ferror(tf) );
  assert( feof(tf) );
  assert( l>1 );
  l--;
  assert( lbuf[l]=='\n' );
  lbuf[l]= 0;

  sysassert(! setenv("YPPSC_DATA_TIMESTAMP",lbuf,1) );
  fclose(tf);
}

static void set_server(const char *envname, const char *defprotocol,
		       const char *defvalue, const char *defvalue_test,
		       const char *userspecified,
		       int enable) {
  const char *value;
  
  if (!enable) { value= "0"; goto ok; }

  if (userspecified)
    value= userspecified;
  else if ((value= getenv(envname)))
    ;
  else if (o_flags & ff_testservers)
    value= defvalue_test;
  else
    value= defvalue;

  if (value[0]=='/' || (value[0]=='.' && value[1]=='/'))
    /* absolute or relative pathname - or anyway, something with no hostname */
    goto ok;

  const char *colon= strchr(value, ':');
  const char *slash= strchr(value, '/');

  if (colon && (!slash || colon < slash))
    /* colon before the first slash, if any */
    /* rsync :: protocol specification - anyway, adding scheme:// won't help */
    goto ok;

  int vallen= strlen(value);

  value= masprintf("%s%s%s", defprotocol, value,
		   vallen && value[vallen-1]=='/' ? "" : "/");

 ok:
  sysassert(! setenv(envname,value,1) );
}

static void outputmode(enum outmodekind kind, const char *str) {
  OutputMode *om= o_outmodes;
  OutputMode *sentinel= o_outmodes + ARRAYSIZE(o_outmodes) - 1;
  for (;;) {
    if (om==sentinel) badusage("too many output modes specified");
    if (!om->kind) break;
    om++;
  }
  om->kind= kind;
  om->str=  str;
}

static void outputmode_uploads(void) {
  outputmode(omk_upload_yarrg, "upload-yarrg");
  outputmode(omk_upload_pctb, "upload-pctb");
}

int main(int argc, char **argv) {
  const char *arg;

  sysassert( setlocale(LC_MESSAGES,"") );
  sysassert( setlocale(LC_CTYPE,"en_GB.UTF-8") ||
	     setlocale(LC_CTYPE,"en_US.UTF-8") ||
	     setlocale(LC_CTYPE,"en.UTF-8") );

#define ARGVAL  ((*++argv) ? *argv : \
		 (badusage("missing value for option %s",arg),(char*)0))

#define IS(s) (!strcmp(arg,(s)))

  while ((arg=*++argv)) {
    if (IS("--find-window-only"))      o_mode= mode_findwindow;
    else if (IS("--screenshot-only"))  o_mode= mode_screenshot;
    else if (IS("--show-charset"))     o_mode= mode_showcharset;
    else if (IS("--analyse-only") ||
	     IS("--same"))             o_mode= mode_analyse;
    else if (IS("--everything"))       o_mode= mode_all;
    else if (IS("--find-island"))      o_flags |= ffs_printisland;
    else if (IS("--single-page"))      o_flags |= ff_singlepage;
    else if (IS("--quiet"))            o_quiet= 1;
    else if (IS("--edit-charset"))     o_flags |= ff_charset_edit;
    else if (IS("--no-edit-charset"))  o_flags &= ~(ffm_charset);
    else if (IS("--test-servers"))     o_flags |= ff_testservers;
    else if (IS("--dict-local-only"))  o_flags &= ~ffs_dict;
    else if (IS("--dict-read-only"))   o_flags &= (~ffs_dict | ff_dict_fetch);
    else if (IS("--dict-anon"))        o_flags &= ~ff_dict_pirate;
    else if (IS("--dict-submit"))      o_flags |= ff_dict_fetch|ff_dict_submit;
    else if (IS("--dict-no-update"))   o_flags &= ~ff_dict_fetch; // testing
    else if (IS("--raw-tsv"))          outputmode(omk_raw,0);
    else if (IS("--upload"))           outputmode_uploads();
    else if (IS("--upload-yarrg"))     outputmode(omk_upload_yarrg,arg+2);
    else if (IS("--upload-pctb"))      outputmode(omk_upload_pctb,arg+2);
    else if (IS("--arbitrage") ||
	     IS("--tsv") ||
	     IS("--best-prices"))      outputmode(omk_str,arg+2);
    else if (IS("--screenshot-file")||
	     IS("--screenshots-file")) o_screenshot_fn= ARGVAL;
    else if (IS("--yarrg-server"))        o_serv_yarrg=       ARGVAL;
    else if (IS("--pctb-server"))         o_serv_pctb=        ARGVAL;
    else if (IS("--dict-submit-server"))  o_serv_dict_submit= ARGVAL;
    else if (IS("--dict-update-server"))  o_serv_dict_fetch=  ARGVAL;
    else if (IS("--ocean"))            o_ocean=  ARGVAL;
    else if (IS("--pirate"))           o_pirate= ARGVAL;
#define DF(f)					\
    else if (IS("-D" #f))			\
      debug_flags |= dbg_##f;
    DEBUG_FLAG_LIST
#undef DF
    else if (IS("--window-id")) {
      char *ep;
      unsigned long windowid= strtoul(ARGVAL,&ep,0);
      if (*ep) badusage("invalid window id");
      set_yppclient_window(windowid);
    } else
      badusage("unknown option `%s'",arg);
  }

  /* Consequential changes to options */

  if (o_mode & mf_analyse) {
    if (!o_outmodes[0].kind) {
      if (o_flags & ff_printisland) {
	o_flags |= ff_singlepage;
      } else {
	outputmode_uploads();
      }
    }
  } else {
    if (o_outmodes[0].kind)
      badusage("overall mode does not include analysis but output option(s)"
	       " (eg `--%s') specified",  o_outmodes[0].str);
  }

  OutputMode *om;
  for (om=o_outmodes; om->kind; om++) {
    switch (om->kind) {
    case omk_upload_yarrg: o_flags |= ffs_upload | ff_use_yarrg; break;
    case omk_upload_pctb:  o_flags |= ffs_upload | ff_use_pctb;  break;
    default: ;
    }
  }

  if ((o_flags & (ff_needisland|ff_upload)) &&
      !(o_flags & (ffm_use)))
    o_flags |= ffm_use; /* all */

  if (o_serv_yarrg && !o_serv_dict_submit)
    o_serv_dict_submit= o_serv_yarrg;
  
  /* Defaults */

  set_server("YPPSC_YARRG_YARRG",
	     "http://",          "upload.yarrg.chiark.net",
	                         "upload.yarrg.chiark.net/test",
	     o_serv_yarrg,       o_flags & ff_use_yarrg);

  set_server("YPPSC_YARRG_PCTB",
	     "http://",          "pctb.crabdance.com",
	                         "pctb.ilk.org",
	     o_serv_pctb,        o_flags & ff_use_pctb);
	     
  set_server("YPPSC_YARRG_DICT_UPDATE",
	     "rsync://",         "rsync.yarrg.chiark.net/yarrg",
	                         "rsync.yarrg.chiark.net/yarrg/test",
	     o_serv_dict_fetch,   o_flags & ff_dict_fetch);

  set_server("YPPSC_YARRG_DICT_SUBMIT",
	     "http://",           "upload.yarrg.chiark.net",
	                          "upload.yarrg.chiark.net/test",
	     o_serv_dict_submit,  o_flags & ff_dict_submit);

  if (!o_screenshot_fn)
    o_screenshot_fn= masprintf("%s/_pages.ppm.gz", get_vardir());

  /* Actually do the work */

  if ((o_flags & ff_upload) && (o_flags & ff_use_yarrg))
    get_timestamp();
	     
  canon_colour_prepare();
  
  if (o_mode & mf_findwindow) {
    screenshot_startup();
    find_yppclient_window();
  }
  if (!ocean)  ocean=  o_ocean;
  if (!pirate) pirate= o_pirate;
  
  if (o_flags & ff_needisland)
    if (!ocean)
      badusage("need --ocean option when not using actual YPP client window"
	       " (consider supplying --pirate too)");
  if (ocean)
    sysassert(! setenv("YPPSC_OCEAN",ocean,1) );
  if (pirate && (o_flags & ff_dict_pirate))
    sysassert(! setenv("YPPSC_PIRATE",pirate,1) );

  switch (o_mode & mfm_special) {
  case 0:                                      break;
  case mode_showcharset:  ocr_showcharsets();  exit(0);
  default:                                     abort();
  }

  if (o_mode & mf_printoceanpirate)
    printf("%s %s\n",ocean,pirate);

  if (o_mode & mf_screenshot) {
    open_screenshot_file(1);
    if (o_flags & ff_singlepage) take_one_screenshot();
    else take_screenshots();
    progress_log("OK for you to move the mouse now, and you can"
		 " use the YPP client again.");
    progress("Finishing handling screenshots...");
    gzclose(&screenshot_file,&screenshot_compressor,"screenshots output");
  }
  if (o_mode & mf_readscreenshot) {
    if ((o_flags & ff_upload) && !(o_flags & ff_testservers))
      badusage("must not reuse screenshots for upload to live databases");
    open_screenshot_file(0);
    if (o_flags & ff_singlepage) read_one_screenshot();
    else read_screenshots();
    gzclose(&screenshot_file,&screenshot_compressor,"screenshots input");
  }
  if (o_mode & mf_analyse) {
    if (o_flags & ff_needisland) {
      find_islandname();
      if (o_flags & ff_printisland)
	printf("%s, %s\n", archipelago, island);
      sysassert(! setenv("YPPSC_ISLAND",island,1) );
    }
    if (o_outmodes[0].kind==omk_raw && o_outmodes[1].kind==omk_unset)
      analyse(stdout);
    else
      run_analysis();
  }
  progress_log("Finished.");
  return 0;
}


FILE *dbfile;
static const char *basepath; /* as passed in by caller */
static pid_t dbzcat;

int dbfile_gzopen(const char *basepath_spec) {
  assert(!dbfile);

  basepath= basepath_spec;

  char *zpath= masprintf("%s.gz", basepath);
  int e= gzopen(zpath, O_RDONLY, &dbfile, &dbzcat, 0);
  free(zpath);
  if (e) { errno=e; sysassert(errno==ENOENT); return 0; }
  
  return 1;
}  

int dbfile_open(const char *tpath) {
  assert(!dbfile);

  basepath= tpath;

  dbzcat= -1;
  dbfile= fopen(tpath,"r");
  if (!dbfile) { sysassert(errno==ENOENT); return 0; }
  return 1;
}  

void dbfile_close(void) {
  gzclose(&dbfile, &dbzcat, basepath);
}

#define dbassertgl(x) ((x) ? (void)0 : dbfile_assertfail(file,line,#x))

void dbfile_getsline(char *lbuf, size_t lbufsz, const char *file, int line) {
  errno=0;
  char *s= fgets(lbuf,lbufsz,dbfile);
  sysassert(!ferror(dbfile));
  dbassertgl(!feof(dbfile));
  assert(s);
  int l= strlen(lbuf);
  dbassertgl(l>0);  dbassertgl(lbuf[--l]=='\n');
  lbuf[l]= 0;
}

int dbfile_vscanf(const char *fmt, va_list al) {
  int r= vfscanf(dbfile,fmt,al);
  sysassert(!ferror(dbfile));
  return r;
}

int dbfile_scanf(const char *fmt, ...) {
  va_list al;
  va_start(al,fmt);
  int r= dbfile_vscanf(fmt,al);
  va_end(al);
  return r;
}

void dbfile_assertfail(const char *file, int line, const char *m) {
  if (dbzcat)
    fatal("Error in dictionary file %s.gz:\n"
	  " Requirement not met at %s:%d:\n"
	  " %s",
	  basepath, file,line, m);
  else if (dbfile)
    fatal("Error in dictionary file %s at byte %ld:\n"
	  " Requirement not met at %s:%d:\n"
	  " %s",
	  basepath,(long)ftell(dbfile), file,line, m);
  else
    fatal("Semantic error in dictionaries:\n"
	  " Requirement not met at %s:%d:\n"
	  " %s",
	  file,line, m);
}

int gzopen(const char *zpath, int oflags, FILE **f_r, pid_t *pid_r,
	   const char *gziplevel /* 0 for read; may be 0, or "-1" etc. */) {

  int zfd= open(zpath, oflags, 0666);
  if (zfd<0) return errno;

  int pipefds[2];
  sysassert(! pipe(pipefds) );

  int oi,io; const char *cmd; const char *stdiomode;
  switch ((oflags & O_ACCMODE)) {
  case O_RDONLY: oi=0; io=1; cmd="gunzip"; stdiomode="r"; break;
  case O_WRONLY: oi=1; io=0; cmd="gzip";   stdiomode="w"; break;
  default: abort();
  }

  sysassert( (*pid_r=fork()) != -1 );
  if (!*pid_r) {
    sysassert( dup2(zfd,oi)==oi );
    sysassert( dup2(pipefds[io],io)==io );
    sysassert(! close(zfd) );
    sysassert(! close(pipefds[0]) );
    sysassert(! close(pipefds[1]) );
    execlp(cmd,cmd,gziplevel,(char*)0);
    sysassert(!"execlp gzip/gunzip");
  }
  sysassert(! close(zfd) );
  sysassert(! close(pipefds[io]) );
  sysassert( *f_r= fdopen(pipefds[oi], stdiomode) );

  return 0;
}

void gzclose(FILE **f, pid_t *p, const char *what) {
  if (!*f) return;
  
  sysassert(!ferror(*f));
  sysassert(!fclose(*f));

  if (*p != -1) {
    char *process= masprintf("%s (de)compressor",what);
    waitpid_check_exitstatus(*p,process,1);
    free(process);
    *p= -1;
  }

  *f= 0;
}
