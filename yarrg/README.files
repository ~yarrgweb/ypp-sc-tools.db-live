Files we use and update
-----------------------

The program reads and writes the following files:

 * _pages.ppm.gz

   Contains one or more images (as raw ppms, end-to-end) which are the
   screenshots taken in the last run.  This is (over)written whenever
   we take screenshots from the YPP client.  You can reprocess an
   existing set of screenshots with the --same (aka --analyse-only)
   option; in that case we just read the screenshots file.

   You can specify a different file with --screenshot-file.

   If you want to display the contents of this file, `display' can do
   it, although you may have to uncompress it first.  Don't try
   `display vid:_pages.ppm' as this will consume truly stupendous
   quantities of RAM - it wedged my laptop.

 * _master-newcommods.txt _local-newcommods.txt

   Dictionary of newly introduced commodities.  When a new commodity
   appears in Puzzle Pirates, the YARRG and PCTB server operators each
   need to add it to their database for us to be able to upload data
   about it.

   It can sometimes take a few days to do this.  In the meantime, it
   is possible to upload partial data - data just omitting that
   commodity.  This is controlled by these files: they list
   commodities which should be automatically ignored if the server
   doesn't know about them.  The master file is downloaded and updated
   automatically from my server.  You may create the local file
   yourself.  The format is simple: one commodity per line.

   Unrecognised commodities can also be due to OCR failure so
   double-check what you're doing before overriding the uploader by
   telling it to ignore an unrecognised commodity.

 * _master-info*.txt _local-info.txt

   Database of valid commodities and islands/oceans for use when
   uploading to YARRG.

 * _master-reject.txt _local-reject.txt

   Dictionary of regexps which, when the OCR appears to match, we
   reject instead.  At the moment this is used to stop us thinking
   that `Butterfly weed' is `Butterflyweed'.  This happens if the
   character set dictionary is missing the lowercase `y ' glyph.
   See README.charset.

 * _master-char*.txt      _local-char*.txt
   _master-pixmap.txt.gz  _local-pixmap.txt

   Character set and image dictionaries.  For the semantics of the
   char* files README.charset.  There is not currently any accurate
   documentation of this dictionary format.

   _master-*.txt contain the centrally defined and approved data.
   They are downloaded automatically from the SC YARRG server and
   updated each run.  You can safely delete these files, if everything
   is online, if you want to fetch a fresh copy.

   _local-*.txt are a local copy of your submissions, so that they
   will be used by your client pending approval by me.  You can delete
   this file if you think you may have made a mistake.

   See README.privacy for details of the communications with the SC
   server about the contents of these dictionaries.

 * _commodmap.tsv

   Map from commodity names to the numbers required by the PCTB
   server.  This is fetched and updated automatically as necessary.
   It can safely be deleted as it will then be refetched.

 * _upload-1.html _upload-2.html _upload-3.html

   We HTML-screenscrape the pages from the PCTB upload server.  The
   actual HTML returned from the upload server is left in these
   dropping files for debugging etc.

 * _<file>.tmp

   When any of these tools overwrite one of the persistent dictionary
   files, they temporarily write to _<file>.tmp.  We also use a couple
   of other temporary files.

Future versions may have more helpers and more data files.
